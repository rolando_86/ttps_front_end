import { Component } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { GastoService } from '../service/gasto.service';

@Component({
  selector: 'app-gasto',
  templateUrl: './gasto.component.html',
  styleUrl: './gasto.component.scss'
})
export class GastoComponent {
  public listaGasto!:any[];

  constructor(public gastoService:GastoService,private authService: AuthService,public cookieService: CookieService, private router: Router){
    if(this.cookieService.get('login-status')==""){
      this.router.navigate(['login']);
    }
    this.obtenerGastos();
  }

  logout(){
    this.authService.eliminarUsuarioEnLocalStorage();
    this.cookieService.set('login-status', '');
    this.router.navigate(['login']);
  }

  redirectGasto(){
    this.router.navigate(['gasto']);
  }

  redirectGrupo(){
    this.router.navigate(['grupo']);
  }

  crearGasto(){
    this.router.navigate(['crear-gasto']);
  }

  listaGastosGrupal:any;
  listaGastosIndividual:any;
  obtenerGastos(){
    let usuario = this.authService.obtenerUsuarioDesdeLocalStorage();
    console.log(usuario);
    this.gastoService.obtenerGastoGrupal(usuario.idUsuario).subscribe({
      next: (data) =>{
        console.log(data);
        this.listaGastosGrupal = data;
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      },
    });
  }

  
  editaGasto(gastoSeleccionado:string){
    this.router.navigate(['editar-gasto',btoa(JSON.stringify(gastoSeleccionado))]);
  }
  
}
