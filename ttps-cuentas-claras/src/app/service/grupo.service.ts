import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GrupoService {

  constructor(private http: HttpClient) { }

  obtenerGrupos(idUsuario: string): Observable<any>{
    return this.http.get("/api/GrupoAmigos/" +idUsuario);
  }

  crearGrupo(idCategoria:string,nombreGrupo:string, idUsuario:string): Observable<any>{
    return this.http.post("/api/GrupoAmigos/"+idCategoria+"/"+nombreGrupo+"/"+idUsuario,null);
  }

  editarGrupo(idGrupo:string,nombreGrupo:string, idCategoria:string):Observable<any>{
    let grupo = {
      idCategoria: idCategoria,
      nombreGrupo: nombreGrupo
  };
    return this.http.put("/api/GrupoAmigos/"+idGrupo,grupo);

  }
}
