import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaGastoService {

  constructor(private http: HttpClient) { }

  obtenerCategoriasGastos(): Observable<any>{
    return this.http.get("/api/CategoriaGasto");
  }

}
