import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoriaGrupoService {

  constructor(private http: HttpClient) { }

  obtenerCategoriasGrupos(): Observable<any>{
    return this.http.get("/api/CategoriaGrupo");
  }

}
