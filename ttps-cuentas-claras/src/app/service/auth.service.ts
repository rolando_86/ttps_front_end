import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private localStorageKey = 'usuario';

  // Método para guardar el usuario en el localStorage
  guardarUsuarioEnLocalStorage(usuario: any): void {
    localStorage.setItem(this.localStorageKey, JSON.stringify(usuario));
  }

  // Método para recuperar el usuario del localStorage
  obtenerUsuarioDesdeLocalStorage(): any {
    const usuarioString = localStorage.getItem(this.localStorageKey);
    return usuarioString ? JSON.parse(usuarioString) : null;
  }

  eliminarUsuarioEnLocalStorage():any{
    localStorage.removeItem(this.localStorageKey);
  }

  // Otros métodos de autenticación...
}
