import { TestBed } from '@angular/core/testing';

import { CategoriaGrupoService } from './categoria-grupo.service';

describe('CategoriaGrupoService', () => {
  let service: CategoriaGrupoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CategoriaGrupoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
