import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GastoService {

  constructor(private http: HttpClient) { }

  obtenerGastoGrupal(idUsuario:string): Observable<any>{
    return this.http.get("/api/Gasto/GastoGrupal/"+idUsuario);
  }

  crearGasto(gasto:any): Observable<any>{
    return this.http.post("/api/Gasto",gasto);
  }

  editarGasto(idGasto:string,gasto:any): Observable<any>{
    return this.http.put("/api/Gasto/"+idGasto,gasto);
  }

}
