import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { HomeComponent } from './home/home.component';
import { GastoComponent } from './gasto/gasto.component';
import { GrupoComponent } from './grupo/grupo.component';
import { CrearGrupoComponent } from './crear-grupo/crear-grupo.component';
import { EditarGrupoComponent } from './editar-grupo/editar-grupo.component';
import { CrearGastoComponent } from './crear-gasto/crear-gasto.component';
import { EditarGastoComponent } from './editar-gasto/editar-gasto.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: "login", component: LoginComponent, pathMatch: "full" },
  { path: "registrar", component: RegistrarComponent, pathMatch: "full" },
  { path: "home", component: HomeComponent, pathMatch: "full" },
  { path: "gasto", component: GastoComponent, pathMatch: "full" },
  { path: "grupo", component: GrupoComponent, pathMatch: "full" },
  { path: "crear-grupo", component: CrearGrupoComponent, pathMatch: "full" },
  { path: "editar-grupo/:id", component: EditarGrupoComponent, pathMatch: "full" },
  { path: "crear-gasto", component: CrearGastoComponent, pathMatch: "full" },
  { path: "editar-gasto/:id", component: EditarGastoComponent, pathMatch: "full" }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
