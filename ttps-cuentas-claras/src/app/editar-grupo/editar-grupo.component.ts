import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../service/auth.service';
import { GrupoService } from '../service/grupo.service';
import { CategoriaGrupoService } from '../service/categoria-grupo.service';

@Component({
  selector: 'app-editar-grupo',
  templateUrl: './editar-grupo.component.html',
  styleUrl: './editar-grupo.component.scss'
})
export class EditarGrupoComponent {

  nombreGrupo!:string;
  categoria!:string;

  listaCategoriasGrupo!:any[];

  constructor(public categoriaService:CategoriaGrupoService, public grupoService:GrupoService, private routeActive: ActivatedRoute, private authService: AuthService,public cookieService: CookieService, private router: Router){
    if(this.cookieService.get('login-status')==""){
      this.router.navigate(['login']);
    }
  }

  private idGrupo!:any;

  ngOnInit(){
    this.categoriaService.obtenerCategoriasGrupos().subscribe({
      next: (data) => {
        console.log(data);
        this.listaCategoriasGrupo = data;  
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      }
    })

    const objetoString:any = this.routeActive.snapshot.paramMap.get('id');
    
    let grupo = atob(objetoString);
    let obj:any = JSON.parse(grupo);
    console.log(obj);
    this.nombreGrupo = obj.nombreGrupo;
    this.categoria = obj.categoria.idCategoriaGrupos;
    this.idGrupo = obj.idGrupoAmigos;
  }

  logout(){
    this.authService.eliminarUsuarioEnLocalStorage();
    this.cookieService.set('login-status', '');
    this.router.navigate(['login']);
  }

  redirectGasto(){
    this.router.navigate(['gasto']);
  }

  redirectGrupo(){
    this.router.navigate(['grupo']);
  }

  editar(){
    alert("Editado");
    console.log(this.nombreGrupo);
    console.log(this.categoria);
    let usuario = this.authService.obtenerUsuarioDesdeLocalStorage();
    console.log(usuario);

    this.grupoService.editarGrupo(this.idGrupo,this.nombreGrupo,this.categoria).subscribe({
      next: (data) => {
        console.log(data);
        alert("Se edito correctamente" + data.nombreGrupo);
        this.router.navigate(['grupo']);
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      }
    });

  }

  cancelar(){
    this.router.navigate(['grupo']);
  }
}
