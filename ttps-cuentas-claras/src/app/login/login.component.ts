import { Component } from '@angular/core';
import { UserService } from '../service/user.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  
  email!: string;
  password!: string;

  constructor(private authService: AuthService, private router: Router,public userService: UserService, public cookieService: CookieService) {
    if(this.cookieService.get('login-status')==""){
        console.log("validando");
    }else{
      this.router.navigate(['home']);
    }
  }


  login(){
    console.log(this.email);
    console.log(this.password);
    const user = { email: this.email, contrasena: this.password };
    this.userService.login(user).subscribe({
      next: (data) => {
        console.log(data);
        this.cookieService.set('login-status', 'ok');
        this.authService.guardarUsuarioEnLocalStorage(data);
        this.router.navigate(['home']);

      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        if(errorData.status == 403) alert(errorData.error);
      }
    });
  }

  registrar(){
    this.router.navigate(['registrar']);
  }

}
