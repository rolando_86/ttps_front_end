import { Component } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { GrupoService } from '../service/grupo.service';

@Component({
  selector: 'app-grupo',
  templateUrl: './grupo.component.html',
  styleUrl: './grupo.component.scss'
})
export class GrupoComponent {

  listaGrupos:any;

  constructor(public grupoService:GrupoService,private authService: AuthService,public cookieService: CookieService, private router: Router){
    if(this.cookieService.get('login-status')==""){
      this.router.navigate(['login']);
    }
    this.obtenerGrupos();
  }
  

  logout(){
    this.authService.eliminarUsuarioEnLocalStorage();
    this.cookieService.set('login-status', '');
    this.router.navigate(['login']);
  }

  redirectGasto(){
    this.router.navigate(['gasto']);
  }

  redirectGrupo(){
    this.router.navigate(['grupo']);
  }

  crearGrupo(){
    this.router.navigate(['crear-grupo']);
  }

  obtenerGrupos(){
    let usuario = this.authService.obtenerUsuarioDesdeLocalStorage();
    console.log(usuario);
    this.grupoService.obtenerGrupos(usuario.idUsuario).subscribe({
      next: (data) => {
        console.log(data);
        this.listaGrupos = data;
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      }
    });
  }

  editaGrupo(grupoSeleccionado:string){
    this.router.navigate(['editar-grupo',btoa(JSON.stringify(grupoSeleccionado))]);
  }
}
