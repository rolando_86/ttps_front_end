import { Component } from '@angular/core';
import { UserService } from '../service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrl: './registrar.component.scss'
})
export class RegistrarComponent {
  
  nombreUsuario!:String;
  apellido!:String;
  nombre!:String;
  emailUsuario!:String;
  contrasena!:String;
  confirmacionContrasena!:String;
  saldo:number=0;

  constructor(private router: Router, public userService: UserService){}

  registrarUsuario(){
    console.log(this.nombreUsuario);
    console.log(this.apellido);
    console.log(this.nombre);
    console.log(this.emailUsuario);
    console.log(this.contrasena);
    console.log(this.confirmacionContrasena);
    console.log(this.saldo);
    if(this.contrasena == this.confirmacionContrasena){
        console.log("las contrasenas son correctas");
        const user = { 
            nombreUsuario: this.nombreUsuario, 
            apellido: this.apellido,
            nombres: this.nombre,
            email: this.emailUsuario,
            contrasena: this.contrasena,
            saldoTotal: 0
          };
        this.userService.registrar(user).subscribe({
            next: (data) => {
                console.log(data);
                alert("Registro satisfactorio")
                this.router.navigate(['login']);
            },
            error: (errorData) =>{
                console.log(errorData.status);
                console.log(JSON.stringify(errorData));
                if(errorData.status == 400) alert(errorData.error);
            }
        });
        

    }else{
        alert("La contraseñas no coinciden");
    }
    


  }

  volver(){
    this.router.navigate(['login']);
  }

}
