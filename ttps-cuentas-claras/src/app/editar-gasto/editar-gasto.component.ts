import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../service/auth.service';
import { GastoService } from '../service/gasto.service';
import { CategoriaGastoService } from '../service/categoria-gasto.service';

@Component({
  selector: 'app-editar-gasto',
  templateUrl: './editar-gasto.component.html',
  styleUrl: './editar-gasto.component.scss'
})
export class EditarGastoComponent {

  tipoGasto!:string;
  idCategoriaGastos!:string;
  montoGasto!:number;
  fechaGasto!:string;
  emailGastoIntegrante!:string;
  formaDividirGasto!:string;
  valor!:number;
  nombreGrupo!:string;

  listaCategoriaGasto!:any[];

  constructor(public categoriaService:CategoriaGastoService, public gastoService:GastoService,private routeActive: ActivatedRoute, private authService: AuthService,public cookieService: CookieService, private router: Router){
    if(this.cookieService.get('login-status')==""){
      this.router.navigate(['login']);
    }

    //obtener el grupo
    //setearlo en los input
  }

  private idGasto!:any;

  ngOnInit(){
    this.categoriaService.obtenerCategoriasGastos().subscribe({
      next: (data) => {
        console.log(data);
        this.listaCategoriaGasto = data;
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      }
    });

    const objetoString:any = this.routeActive.snapshot.paramMap.get('id');


    let gasto = atob(objetoString);
    let obj:any = JSON.parse(gasto);
    console.log(obj);

    this.idGasto = obj.idGasto;

    this.tipoGasto = "Grupal";
    this.idCategoriaGastos = obj.categoriaGasto.idCategoriaGastos;
    this.montoGasto = obj.montoGasto;
    this.fechaGasto = obj.fechaGasto;
    this.emailGastoIntegrante = obj.emailGastoIntegrante;
    this.formaDividirGasto = obj.formaDividirGasto;
  }

  logout(){
    this.authService.eliminarUsuarioEnLocalStorage();
    this.cookieService.set('login-status', '');
    this.router.navigate(['login']);
  }

  redirectGasto(){
    this.router.navigate(['gasto']);
  }

  redirectGrupo(){
    this.router.navigate(['grupo']);
  }

  editar(){
    let usuario = this.authService.obtenerUsuarioDesdeLocalStorage();
    console.log(usuario);
    console.log(this.tipoGasto);
    console.log(this.idCategoriaGastos);
    console.log(this.montoGasto);
    console.log(this.fechaGasto);
    console.log(this.emailGastoIntegrante);
    console.log(this.formaDividirGasto);
    console.log(this.valor);
    console.log(this.nombreGrupo);

    let gasto = {
      montoGasto:this.montoGasto,
      fechaGasto:this.fechaGasto,
      imagenAsociada:"",
      formaDividirGasto:this.formaDividirGasto,
      emailGastoIntegrante:this.emailGastoIntegrante,
      idCategoriaGastos:this.idCategoriaGastos,
      idUsuario:usuario.idUsuario,
      nombreGrupo:this.nombreGrupo,
      tipoGasto:this.tipoGasto,
      valor:this.valor
    };
   
    this.gastoService.editarGasto(this.idGasto,gasto).subscribe({
      next: (data) => {
        console.log(data);
        alert("Se edito correctamente: ");
        this.router.navigate(['gasto']);
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      }
    });

  }

  imageSrc: string | ArrayBuffer | null = null;
  selectedImage: File | null = null;
  handleImageChange(event: any) {
    const file = event.target.files[0];
    

    if (file) {
      //const dataURL = this.imageService.encodeImageFileAsURL(file);
      this.selectedImage = file;
      
    }
  }

  cancelar(){
    this.router.navigate(['gasto']);
  }
}
