import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss'
})
export class HomeComponent {

  constructor(private authService: AuthService,public cookieService: CookieService, private router: Router){
    if(this.cookieService.get('login-status')==""){
      this.router.navigate(['login']);
    }
  }

  logout(){
    this.authService.eliminarUsuarioEnLocalStorage();
    this.cookieService.set('login-status', '');
    this.router.navigate(['login']);
  }

  redirectGasto(){
    this.router.navigate(['gasto']);
  }

  redirectGrupo(){
    this.router.navigate(['grupo']);
  }

}
