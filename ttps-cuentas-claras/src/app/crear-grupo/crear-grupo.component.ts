import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from '../service/auth.service';
import { GrupoService } from '../service/grupo.service';
import { CategoriaGrupoService } from '../service/categoria-grupo.service';

@Component({
  selector: 'app-crear-grupo',
  templateUrl: './crear-grupo.component.html',
  styleUrl: './crear-grupo.component.scss'
})
export class CrearGrupoComponent {

  nombreGrupo!:string;
  categoria!:string;

  listaCategoriasGrupo!:any[];

  constructor(public categoriaService:CategoriaGrupoService, public grupoService:GrupoService, private authService: AuthService,public cookieService: CookieService, private router: Router){
    if(this.cookieService.get('login-status')==""){
      this.router.navigate(['login']);
    }
  }

  ngOnInit(){
    this.categoriaService.obtenerCategoriasGrupos().subscribe({
      next: (data) => {
        console.log(data);
        this.listaCategoriasGrupo = data;  
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      }
    })
  }

  logout(){
    this.authService.eliminarUsuarioEnLocalStorage();
    this.cookieService.set('login-status', '');
    this.router.navigate(['login']);
  }

  redirectGasto(){
    this.router.navigate(['gasto']);
  }

  redirectGrupo(){
    this.router.navigate(['grupo']);
  }
  crear(){
    console.log(this.nombreGrupo);
    console.log(this.categoria);
    let usuario = this.authService.obtenerUsuarioDesdeLocalStorage();
    console.log(usuario);
    this.grupoService.crearGrupo(this.categoria,this.nombreGrupo,usuario.idUsuario).subscribe({
      next: (data) => {
        console.log(data);
        alert("Se creo correctamente el grupo: " + data.nombreGrupo);
        this.router.navigate(['grupo']);  
      },
      error: (errorData) => {
        console.log(errorData.status);
        console.log(JSON.stringify(errorData));
        alert("Error interno")
      }
    });
  }

  cancelar(){
    this.router.navigate(['grupo']);
  }

}
