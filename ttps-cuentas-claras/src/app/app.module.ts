import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { HomeComponent } from './home/home.component';
import { CookieService } from 'ngx-cookie-service';
import { GrupoComponent } from './grupo/grupo.component';
import { CrearGrupoComponent } from './crear-grupo/crear-grupo.component';
import { EditarGrupoComponent } from './editar-grupo/editar-grupo.component';
import { GastoComponent } from './gasto/gasto.component';
import { CrearGastoComponent } from './crear-gasto/crear-gasto.component';
import { EditarGastoComponent } from './editar-gasto/editar-gasto.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrarComponent,
    HomeComponent,
    GrupoComponent,
    CrearGrupoComponent,
    EditarGrupoComponent,
    GastoComponent,
    CrearGastoComponent,
    EditarGastoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
